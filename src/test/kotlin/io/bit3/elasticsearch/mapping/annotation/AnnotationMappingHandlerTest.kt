package io.bit3.elasticsearch.mapping.annotation

import io.bit3.elasticsearch.mapping.Mapping
import io.bit3.elasticsearch.mapping.MappingBuilder
import io.bit3.elasticsearch.mapping.MappingHandler
import io.bit3.elasticsearch.mapping.documents.NamedTestDocument
import io.bit3.elasticsearch.mapping.documents.TestDocument
import io.bit3.elasticsearch.mapping.type.AliasDataType
import io.bit3.elasticsearch.mapping.type.ArrayDataType
import io.bit3.elasticsearch.mapping.type.BinaryDataType
import io.bit3.elasticsearch.mapping.type.BooleanDataType
import io.bit3.elasticsearch.mapping.type.ByteDataType
import io.bit3.elasticsearch.mapping.type.DateDataType
import io.bit3.elasticsearch.mapping.type.DateNanosDataType
import io.bit3.elasticsearch.mapping.type.DateRangeDataType
import io.bit3.elasticsearch.mapping.type.DoubleDataType
import io.bit3.elasticsearch.mapping.type.DoubleRangeDataType
import io.bit3.elasticsearch.mapping.type.FloatDataType
import io.bit3.elasticsearch.mapping.type.FloatRangeDataType
import io.bit3.elasticsearch.mapping.type.GeoPointDataType
import io.bit3.elasticsearch.mapping.type.GeoShapeDataType
import io.bit3.elasticsearch.mapping.type.HalfFloatDataType
import io.bit3.elasticsearch.mapping.type.IntegerDataType
import io.bit3.elasticsearch.mapping.type.IntegerRangeDataType
import io.bit3.elasticsearch.mapping.type.IpDataType
import io.bit3.elasticsearch.mapping.type.IpRangeDataType
import io.bit3.elasticsearch.mapping.type.KeywordDataType
import io.bit3.elasticsearch.mapping.type.LongDataType
import io.bit3.elasticsearch.mapping.type.LongRangeDataType
import io.bit3.elasticsearch.mapping.type.NestedDataType
import io.bit3.elasticsearch.mapping.type.ObjectDataType
import io.bit3.elasticsearch.mapping.type.ScaledFloatDataType
import io.bit3.elasticsearch.mapping.type.ShortDataType
import io.bit3.elasticsearch.mapping.type.TokenCountDataType
import junit.framework.Assert.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import kotlin.reflect.KClass
import kotlin.reflect.full.memberProperties
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

internal class AnnotationMappingHandlerTest {

    private lateinit var mapping: Mapping
    private lateinit var handler: MappingHandler
    private lateinit var builder: MappingBuilder

    @BeforeEach
    fun beforeEach() {
        mapping = Mapping(TestDocument::class)
        handler = AnnotationMappingHandler()
        builder = MappingBuilder(listOf(handler))
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testAliasDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "alias" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("alias", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is AliasDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testArrayDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "array" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("array", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is ArrayDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testBinaryDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "binary" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("binary", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is BinaryDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testBooleanDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "boolean" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("boolean", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is BooleanDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testByteDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "byte" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("byte", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is ByteDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testDateDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "date" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("date", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is DateDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testDateNanosDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "dateNanos" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("dateNanos", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is DateNanosDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testDateRangeDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "dateRange" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("dateRange", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is DateRangeDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testDoubleDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "double" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("double", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is DoubleDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testDoubleRangeDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "doubleRange" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("doubleRange", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is DoubleRangeDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testFloatDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "float" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("float", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is FloatDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testFloatRangeDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "floatRange" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("floatRange", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is FloatRangeDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testGeoPointDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "geoPoint" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("geoPoint", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is GeoPointDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testGeoShapeDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "geoShape" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("geoShape", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is GeoShapeDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testHalfFloatDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "halfFloat" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("halfFloat", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is HalfFloatDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testIntegerDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "integer" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("integer", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is IntegerDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testIntegerRangeDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "integerRange" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("integerRange", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is IntegerRangeDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testIpDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "ip" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("ip", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is IpDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testIpRangeDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "ipRange" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("ipRange", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is IpRangeDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testKeywordDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "keyword" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("keyword", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is KeywordDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testLongDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "long" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("long", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is LongDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testLongRangeDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "longRange" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("longRange", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is LongRangeDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testNestedDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "nested" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("nested", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is NestedDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testObjectDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "object" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("object", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is ObjectDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testScaledFloatDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "scaledFloat" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("scaledFloat", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is ScaledFloatDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testShortDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "short" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("short", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is ShortDataType }
    }

    @ParameterizedTest
    @MethodSource("arguments")
    internal fun testTokenCountDataType(named: Boolean, type: KClass<*>) {
        val property = type.memberProperties.find { "tokenCount" == it.name }!!
        val pair = handler.handleProperty(property, null, null, mapping, builder)

        assertNotNull(pair)
        if (named) {
            assertEquals("tokenCount", pair.first)
        } else {
            assertNull(pair.first)
        }
        assertTrue { pair.second is TokenCountDataType }
    }

    companion object {
        @JvmStatic
        fun arguments() = listOf(
                Arguments.of(false, TestDocument::class),
                Arguments.of(true, NamedTestDocument::class)
        )
    }

}