package io.bit3.elasticsearch.mapping

import io.bit3.elasticsearch.mapping.documents.TestDocument
import io.bit3.elasticsearch.mapping.settings.Analysis
import io.bit3.elasticsearch.mapping.settings.Analyzer
import io.bit3.elasticsearch.mapping.settings.Settings
import io.bit3.elasticsearch.mapping.type.*
import junit.framework.Assert.assertEquals
import org.elasticsearch.common.xcontent.XContentFactory
import org.junit.jupiter.api.Test
import java.io.ByteArrayOutputStream

internal class XContentMappingSerializerTest {

    @Test
    fun buildMapping() {
        val expectedJson = XContentMappingSerializerTest::class.java.getResourceAsStream("test_document.mapping.json").bufferedReader().use {
            it.readText().trim()
        }

        val analyzer = mapOf(
                "ngerman" to Analyzer(type = "custom")
        )
        val analysis = Analysis(analyzer = analyzer)
        val settings = Settings(analysis = analysis)
        val properties = mapOf(
                "alias" to AliasDataType(path = "text"),
                "array" to ArrayDataType(),
                "binary" to BinaryDataType(),
                "boolean" to BooleanDataType(),
                "byte" to ByteDataType(),
                "date" to DateDataType(),
                "dateNanos" to DateNanosDataType(),
                "dateRange" to DateRangeDataType(),
                "double" to DoubleDataType(),
                "doubleRange" to DoubleRangeDataType(),
                "float" to FloatDataType(),
                "floatRange" to FloatRangeDataType(),
                "geoPoint" to GeoPointDataType(),
                "geoShape" to GeoShapeDataType(),
                "halfFloat" to HalfFloatDataType(),
                "integer" to IntegerDataType(),
                "integerRange" to IntegerRangeDataType(),
                "ip" to IpDataType(),
                "ipRange" to IpRangeDataType(),
                "keyword" to KeywordDataType(),
                "long" to LongDataType(),
                "longRange" to LongRangeDataType(),
                "nested" to NestedDataType(),
                "object" to ObjectDataType(),
                "scaledFloat" to ScaledFloatDataType(scalingFactor = 1),
                "short" to ShortDataType(),
                "text" to TextDataType(analyzer = "ngerman"),
                "tokenCount" to TokenCountDataType()
        )
        val mapping = Mapping(TestDocument::class, settings = settings, properties = properties)
        val actualJson = ByteArrayOutputStream().use { outputStream ->
            XContentFactory.jsonBuilder(outputStream).prettyPrint().use { builder ->
                XContentMappingSerializer().build(builder, mapping)
            }
            outputStream.toString("UTF-8")
        }

        assertEquals(expectedJson, actualJson)
    }

}