package io.bit3.elasticsearch.mapping

import io.bit3.elasticsearch.mapping.type.DataType
import io.bit3.elasticsearch.mapping.type.TextDataType
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import java.util.concurrent.atomic.AtomicInteger
import kotlin.reflect.KAnnotatedElement
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KProperty
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

internal class MappingBuilderTest {

    @ParameterizedTest
    @ValueSource(classes = [SinglePropertyDocument::class, SingleMethodDocument::class])
    internal fun testHandlerInvocation(clazz: Class<*>) {
        val handleClassCounter = AtomicInteger(0)
        val handlePropertyCounter = AtomicInteger(0)

        val handler1 = object : MappingHandler {
            override fun handleClass(clazz: KClass<*>, mapping: Mapping, builder: MappingBuilder): Mapping {
                assertEquals(1, handleClassCounter.incrementAndGet())
                return mapping
            }

            override fun handleProperty(property: KAnnotatedElement, name: String?, dataType: DataType?, mapping: Mapping, builder: MappingBuilder): Pair<String?, DataType>? {
                if (property is KProperty<*> && "foo" == property.name || property is KFunction<*> && "getFoo" == property.name) {
                    assertEquals(1, handlePropertyCounter.incrementAndGet())
                    return Pair("foo1", TextDataType())
                }
                return null
            }
        }

        val handler2 = object : MappingHandler {
            override fun handleClass(clazz: KClass<*>, mapping: Mapping, builder: MappingBuilder): Mapping {
                assertEquals(2, handleClassCounter.incrementAndGet())
                return mapping
            }

            override fun handleProperty(property: KAnnotatedElement, name: String?, dataType: DataType?, mapping: Mapping, builder: MappingBuilder): Pair<String?, DataType>? {
                if (property is KProperty<*> && "foo" == property.name || property is KFunction<*> && "getFoo" == property.name) {
                    assertEquals(2, handlePropertyCounter.incrementAndGet())
                    return Pair("foo2", TextDataType())
                }
                return null
            }
        }

        val handler3 = object : MappingHandler {
            override fun handleClass(clazz: KClass<*>, mapping: Mapping, builder: MappingBuilder): Mapping {
                assertEquals(3, handleClassCounter.incrementAndGet())
                return mapping
            }

            override fun handleProperty(property: KAnnotatedElement, name: String?, dataType: DataType?, mapping: Mapping, builder: MappingBuilder): Pair<String?, DataType>? {
                if (property is KProperty<*> && "foo" == property.name || property is KFunction<*> && "getFoo" == property.name) {
                    assertEquals(3, handlePropertyCounter.incrementAndGet())
                    return Pair("foo3", TextDataType())
                }
                return null
            }
        }

        val handlers = listOf<MappingHandler>(handler1, handler2, handler3)
        val builder = MappingBuilder(handlers)
        val mapping = builder.buildMapping(clazz)

        assertNotNull(mapping)
        assertEquals(3, handleClassCounter.get())
        assertEquals(3, handlePropertyCounter.get())
        assertEquals(setOf("foo3"), mapping.properties.keys)
    }

    open class SinglePropertyDocument {

        private var foo: String = "foo"

    }

    open class SingleMethodDocument {

        fun getFoo(): String = "foo"

    }

}