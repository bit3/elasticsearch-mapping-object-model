@file:Suppress("unused")

package io.bit3.elasticsearch.mapping.documents

import io.bit3.elasticsearch.mapping.annotation.*
import java.net.InetAddress
import java.time.LocalDateTime

interface TestDocument {

    @AliasDataType(path = "text")
    val alias: String
        get() = text

    @ArrayDataType
    val array: List<String>

    @BinaryDataType
    val binary: ByteArray

    @BooleanDataType
    val boolean: Boolean

    @ByteDataType
    val byte: Byte

    @DateDataType
    val date: LocalDateTime

    @DateNanosDataType
    val dateNanos: LocalDateTime

    @DateRangeDataType
    val dateRange: LocalDateTime

    @DoubleDataType
    val double: Double

    @DoubleRangeDataType
    val doubleRange: Double

    @FloatDataType
    val float: Float

    @FloatRangeDataType
    val floatRange: Float

    @GeoPointDataType
    val geoPoint: Pair<Double, Double>

    @GeoShapeDataType
    val geoShape: Pair<Pair<Double, Double>, Pair<Double, Double>>

    @HalfFloatDataType
    val halfFloat: Float

    @IntegerDataType
    val integer: Int

    @IntegerRangeDataType
    val integerRange: Int

    @IpDataType
    val ip: InetAddress

    @IpRangeDataType
    val ipRange: InetAddress

    @KeywordDataType
    val keyword: String

    @LongDataType
    val long: Long

    @LongRangeDataType
    val longRange: Long

    @NestedDataType
    val nested: NestedTestDocument

    @ObjectDataType
    val `object`: NestedTestDocument

    @ScaledFloatDataType(scalingFactor = 1)
    val scaledFloat: Float

    @ShortDataType
    val short: Short

    @TextDataType
    val text: String

    @TokenCountDataType
    val tokenCount: Int

}
