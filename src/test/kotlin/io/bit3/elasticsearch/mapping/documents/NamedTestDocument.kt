@file:Suppress("unused")

package io.bit3.elasticsearch.mapping.documents

import io.bit3.elasticsearch.mapping.annotation.*
import java.net.InetAddress
import java.time.LocalDateTime

interface NamedTestDocument {

    @AliasDataType(name = "alias", path = "text")
    val alias: String
        get() = text

    @ArrayDataType(name = "array")
    val array: List<String>

    @BinaryDataType(name = "binary")
    val binary: ByteArray

    @BooleanDataType(name = "boolean")
    val boolean: Boolean

    @ByteDataType(name = "byte")
    val byte: Byte

    @DateDataType(name = "date")
    val date: LocalDateTime

    @DateNanosDataType(name = "dateNanos")
    val dateNanos: LocalDateTime

    @DateRangeDataType(name = "dateRange")
    val dateRange: LocalDateTime

    @DoubleDataType(name = "double")
    val double: Double

    @DoubleRangeDataType(name = "doubleRange")
    val doubleRange: Double

    @FloatDataType(name = "float")
    val float: Float

    @FloatRangeDataType(name = "floatRange")
    val floatRange: Float

    @GeoPointDataType(name = "geoPoint")
    val geoPoint: Pair<Double, Double>

    @GeoShapeDataType(name = "geoShape")
    val geoShape: Pair<Pair<Double, Double>, Pair<Double, Double>>

    @HalfFloatDataType(name = "halfFloat")
    val halfFloat: Float

    @IntegerDataType(name = "integer")
    val integer: Int

    @IntegerRangeDataType(name = "integerRange")
    val integerRange: Int

    @IpDataType(name = "ip")
    val ip: InetAddress

    @IpRangeDataType(name = "ipRange")
    val ipRange: InetAddress

    @KeywordDataType(name = "keyword")
    val keyword: String

    @LongDataType(name = "long")
    val long: Long

    @LongRangeDataType(name = "longRange")
    val longRange: Long

    @NestedDataType(name = "nested")
    val nested: NestedTestDocument

    @ObjectDataType(name = "object")
    val `object`: NestedTestDocument

    @ScaledFloatDataType(name = "scaledFloat", scalingFactor = 1)
    val scaledFloat: Float

    @ShortDataType(name = "short")
    val short: Short

    @TextDataType(name = "text")
    val text: String

    @TokenCountDataType(name = "tokenCount")
    val tokenCount: Int

}
