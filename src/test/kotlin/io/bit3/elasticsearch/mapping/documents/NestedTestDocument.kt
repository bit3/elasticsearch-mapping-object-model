package io.bit3.elasticsearch.mapping.documents

import io.bit3.elasticsearch.mapping.annotation.TextDataType

interface NestedTestDocument {

    @TextDataType
    val foo: String

    @TextDataType
    val bar: String

    @TextDataType
    val zap: String

}
