package io.bit3.elasticsearch.mapping.type

class BooleanDataTypeDefaults {
    companion object {
        const val BOOST: Double = 1.0
        const val DOC_VALUES: Boolean = true
        const val INDEX: Boolean = true
        @JvmStatic
        val NULL_VALUE: Boolean? = null
        const val STORE: Boolean = false
    }
}
