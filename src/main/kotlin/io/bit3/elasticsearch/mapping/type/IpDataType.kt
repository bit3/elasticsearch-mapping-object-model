package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class IpDataType(
        val boost: Double = IpDataTypeDefaults.BOOST,
        @JsonProperty("doc_values")
        val docValues: Boolean = IpDataTypeDefaults.DOC_VALUES,
        val index: Boolean = IpDataTypeDefaults.INDEX,
        @JsonProperty("null_value")
        val nullValue: String? = IpDataTypeDefaults.NULL_VALUE,
        val store: Boolean = IpDataTypeDefaults.STORE
) : DataType {
    override val type: String
        get() = "ip"
}
