package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonProperty

class GeoShapeDataTypeDefaults {
    companion object {
        const val TREE: String = "quadtree"
        const val PRECISION: String = "50m"
        @JvmStatic
        val TREE_LEVELS: String? = null
        const val STRATEGY: String = "recursive"
        const val DISTANCE_ERROR_PCT: Double = .025
        const val ORIENTATION: String = "ccw"
        const val POINTS_ONLY: Boolean = false
        const val IGNORE_MALFORMED: Boolean = false
        const val IGNORE_ZVALUE: Boolean = true
    }
}