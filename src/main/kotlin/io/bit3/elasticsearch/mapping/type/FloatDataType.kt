package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class FloatDataType(
        val coerce: Boolean = FloatDataTypeDefaults.COERCE,
        val boost: Double = FloatDataTypeDefaults.BOOST,
        @JsonProperty("doc_values")
        val docValues: Boolean = FloatDataTypeDefaults.DOC_VALUES,
        @JsonProperty("ignore_malformed")
        val ignoreMalformed: Boolean = FloatDataTypeDefaults.IGNORE_MALFORMED,
        val index: Boolean = FloatDataTypeDefaults.INDEX,
        @JsonProperty("null_value")
        val nullValue: String? = FloatDataTypeDefaults.NULL_VALUE,
        val store: Boolean = FloatDataTypeDefaults.STORE
) : DataType {
    override val type: String
        get() = "float"
}
