package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class GeoPointDataType(
        @JsonProperty("ignore_malformed")
        val ignoreMalformed: Boolean = GeoPointDataTypeDefaults.IGNORE_MALFORMED,
        @JsonProperty("ignore_z_value")
        val ignoreZValue: Boolean = GeoPointDataTypeDefaults.IGNORE_ZVALUE,
        @JsonProperty("null_value")
        val nullValue: String? = GeoPointDataTypeDefaults.NULL_VALUE
) : DataType {
    override val type: String
        get() = "geo_point"
}
