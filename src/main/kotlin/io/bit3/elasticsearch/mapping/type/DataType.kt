package io.bit3.elasticsearch.mapping.type

interface DataType {
    val type: String
}
