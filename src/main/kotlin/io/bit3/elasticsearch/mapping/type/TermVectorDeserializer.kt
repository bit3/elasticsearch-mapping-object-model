package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.deser.std.StdDeserializer

class TermVectorDeserializer : StdDeserializer<TermVector>(TermVector::class.java) {

    override fun deserialize(parser: JsonParser, context: DeserializationContext): TermVector? {
        if (JsonToken.VALUE_NULL == parser.currentToken) {
            return null
        }

        return TermVector.valueOf(parser.valueAsString.toUpperCase())
    }

}
