package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class TextDataType(
        val analyzer: String? = TextDataTypeDefaults.ANALYZER,
        val boost: Double = TextDataTypeDefaults.BOOST,
        @JsonProperty("eager_global_ordinals")
        val eagerGlobalOrdinals: Boolean = TextDataTypeDefaults.EAGER_GLOBAL_ORDINALS,
        val fielddata: Boolean = TextDataTypeDefaults.FIELDDATA,
        @JsonProperty("fielddata_frequency_filter")
        val fielddataFrequencyFilter: FielddataFrequencyFilter? = TextDataTypeDefaults.FIELDDATA_FREQUENCY_FILTER,
        val fields: Map<String, DataType> = TextDataTypeDefaults.FIELDS,
        val index: Boolean = TextDataTypeDefaults.INDEX,
        @JsonProperty("index_options")
        val indexOptions: IndexOptions = TextDataTypeDefaults.INDEX_OPTIONS,
        @JsonProperty("index_prefixes")
        val indexPrefixes: IndexPrefixes? = TextDataTypeDefaults.INDEX_PREFIXES,
        @JsonProperty("index_phrases")
        val indexPhrases: Boolean = TextDataTypeDefaults.INDEX_PHRASES,
        val norms: Boolean = TextDataTypeDefaults.NORMS,

        /**
         * The number of fake term position which should be inserted between each element of an array of strings.
         * Defaults to the position_increment_gap configured on the analyzer which defaults to 100.
         * 100 was chosen because it prevents phrase queries with reasonably large slops (less than 100) from matching terms across field values.
         */
        @JsonProperty("position_increment_gap")
        val positionIncrementGap: Double? = TextDataTypeDefaults.POSITION_INCREMENT_GAP,

        /**
         * Whether the field value should be stored and retrievable separately from the _source field. Accepts true or false (default).
         */
        val store: Boolean = TextDataTypeDefaults.STORE,

        /**
         * The analyzer that should be used at search time on analyzed fields. Defaults to the `analyzer` setting.
         */
        @JsonProperty("search_analyzer")
        val searchAnalyzer: String? = TextDataTypeDefaults.SEARCH_ANALYZER,

        /**
         * The analyzer that should be used at search time when a phrase is encountered. Defaults to the `search_analyzer` setting.
         */
        @JsonProperty("search_quote_analyzer")
        val searchQuoteAnalyzer: String? = TextDataTypeDefaults.SEARCH_QUOTE_ANALYZER,

        /**
         * Which scoring algorithm or similarity should be used. Defaults to [Similarity.BM25].
         */
        val similarity: Similarity = TextDataTypeDefaults.SIMILARITY,

        /**
         * Whether term vectors should be stored for an analyzed field. Defaults to no.
         */
        @JsonProperty("term_vector")
        val termVector: TermVector = TextDataTypeDefaults.TERM_VECTOR
) : DataType {
    override val type: String
        get() = "text"
}
