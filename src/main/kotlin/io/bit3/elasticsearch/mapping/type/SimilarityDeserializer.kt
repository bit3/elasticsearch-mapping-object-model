package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.deser.std.StdDeserializer

class SimilarityDeserializer : StdDeserializer<Similarity>(Similarity::class.java) {

    override fun deserialize(parser: JsonParser, context: DeserializationContext): Similarity? {
        if (JsonToken.VALUE_NULL == parser.currentToken) {
            return null
        } else {
            val value = parser.valueAsString
            return Similarity.values().find { it.value == value }
        }
    }

}
