package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class TokenCountDataType(
        /**
         * The analyzer which should be used to analyze the string value. Required. For best performance, use an analyzer without token filters.
         */
        val analyzer: String? = null,

        /**
         * Indicates if position increments should be counted. Set to false if you don’t want to count tokens removed by analyzer filters (like stop). Defaults to true.
         */
        @JsonProperty("enable_position_increments")
        val enablePositionIncrements: Boolean = true,

        /**
         * Mapping field-level query time boosting. Accepts a floating point number, defaults to 1.0.
         */
        val boost: Double = 1.0,

        /**
         * Should the field be stored on disk in a column-stride fashion, so that it can later be used for sorting, aggregations, or scripting? Accepts true (default) or false.
         */
        @JsonProperty("doc_values")
        val docValues: Boolean = false,

        /**
         * Should the field be searchable? Accepts true (default) and false.
         */
        val index: Boolean = true,

        /**
         * Accepts a numeric value of the same type as the field which is substituted for any explicit null values.
         * Defaults to null, which means the field is treated as missing.
         */
        @JsonProperty("null_value")
        val nullValue: Number? = null,

        /**
         * Whether the field value should be stored and retrievable separately from the _source field. Accepts true or false (default).
         */
        val store: Boolean = false
) : DataType {
    override val type: String
        get() = "token_count"
}
