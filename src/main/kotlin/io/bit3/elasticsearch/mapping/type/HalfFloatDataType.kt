package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class HalfFloatDataType(
        val coerce: Boolean = HalfFloatDataTypeDefaults.COERCE,
        val boost: Double = HalfFloatDataTypeDefaults.BOOST,
        @JsonProperty("doc_values")
        val docValues: Boolean = HalfFloatDataTypeDefaults.DOC_VALUES,
        @JsonProperty("ignore_malformed")
        val ignoreMalformed: Boolean = HalfFloatDataTypeDefaults.IGNORE_MALFORMED,
        val index: Boolean = HalfFloatDataTypeDefaults.INDEX,
        @JsonProperty("null_value")
        val nullValue: String? = HalfFloatDataTypeDefaults.NULL_VALUE,
        val store: Boolean = HalfFloatDataTypeDefaults.STORE
) : DataType {
    override val type: String
        get() = "half_float"
}
