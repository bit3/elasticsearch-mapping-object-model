package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class ObjectDataType(
        val dynamic: Boolean = true,
        val enabled: Boolean = true,
        val properties: Map<String, DataType> = emptyMap()
) : DataType {
    override val type: String
        get() = "object"
}
