package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize

@JsonSerialize(using = SimilaritySerializer::class)
@JsonDeserialize(using = SimilarityDeserializer::class)
enum class Similarity(val value: String) {
    BM25("BM25"),
    CLASSIC("classic"),
    BOOLEAN("boolean");

    override fun toString(): String {
        return value
    }
}
