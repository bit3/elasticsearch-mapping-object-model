package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class FielddataFrequencyFilter(
        val min: Double? = FielddataFrequencyFilterDefaults.MIN,
        val max: Double? = FielddataFrequencyFilterDefaults.MAX,
        @JsonProperty("min_segment_size")
        val minSegmentSize: Int? = FielddataFrequencyFilterDefaults.MIN_SEGMENT_SIZE
)
