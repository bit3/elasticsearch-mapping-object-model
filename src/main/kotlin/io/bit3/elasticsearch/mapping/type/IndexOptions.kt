package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize

@JsonSerialize(using = IndexOptionsSerializer::class)
@JsonDeserialize(using = IndexOptionsDeserializer::class)
enum class IndexOptions {
    DOCS,
    FREQS,
    POSITIONS,
    OFFSETS
}
