package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

class IndexOptionsSerializer : StdSerializer<IndexOptions>(IndexOptions::class.java) {

    override fun serialize(value: IndexOptions?, generator: JsonGenerator, provider: SerializerProvider) {
        if (null == value) {
            generator.writeNull()
        } else {
            generator.writeString(value.toString().toLowerCase())
        }
    }

}
