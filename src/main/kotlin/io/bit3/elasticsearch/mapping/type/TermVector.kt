package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize

@JsonSerialize(using = TermVectorSerializer::class)
@JsonDeserialize(using = TermVectorDeserializer::class)
enum class TermVector {
    /**
     * No term vectors are stored. (default)
     */
    NO,

    /**
     * Just the terms in the field are stored.
     */
    YES,

    /**
     * Terms and positions are stored.
     */
    WITH_POSITIONS,

    /**
     * Terms and character offsets are stored.
     */
    WITH_OFFSETS,

    /**
     * Terms, positions, and character offsets are stored.
     */
    WITH_POSITIONS_OFFSETS
}
