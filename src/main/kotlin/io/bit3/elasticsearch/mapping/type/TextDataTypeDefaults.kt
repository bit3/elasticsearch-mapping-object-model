package io.bit3.elasticsearch.mapping.type

class TextDataTypeDefaults {
    companion object {
        @JvmStatic
        val ANALYZER: String? = null
        const val BOOST: Double = 1.0
        const val EAGER_GLOBAL_ORDINALS: Boolean = false
        const val FIELDDATA: Boolean = false
        @JvmStatic
        val FIELDDATA_FREQUENCY_FILTER: FielddataFrequencyFilter? = null
        @JvmStatic
        val FIELDS: Map<String, DataType> = emptyMap()
        const val INDEX: Boolean = true
        @JvmStatic
        val INDEX_OPTIONS: IndexOptions = IndexOptions.POSITIONS
        @JvmStatic
        val INDEX_PREFIXES: IndexPrefixes? = IndexPrefixes()
        const val INDEX_PHRASES: Boolean = false
        const val NORMS: Boolean = true
        @JvmStatic
        val POSITION_INCREMENT_GAP: Double? = null
        const val STORE: Boolean = false
        @JvmStatic
        val SEARCH_ANALYZER: String? = null
        @JvmStatic
        val SEARCH_QUOTE_ANALYZER: String? = null
        @JvmStatic
        val SIMILARITY: Similarity = Similarity.BM25
        @JvmStatic
        val TERM_VECTOR: TermVector = TermVector.NO
    }
}