package io.bit3.elasticsearch.mapping.type

class ArrayDataTypeDefaults {
    companion object {
        const val DYNAMIC: Boolean = true

        @JvmStatic
        val PROPERTIES: Map<String, DataType> = emptyMap()
    }
}