package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class IntegerDataType(
        val coerce: Boolean = IntegerDataTypeDefaults.COERCE,
        val boost: Double = IntegerDataTypeDefaults.BOOST,
        @JsonProperty("doc_values")
        val docValues: Boolean = IntegerDataTypeDefaults.DOC_VALUES,
        @JsonProperty("ignore_malformed")
        val ignoreMalformed: Boolean = IntegerDataTypeDefaults.IGNORE_MALFORMED,
        val index: Boolean = IntegerDataTypeDefaults.INDEX,
        @JsonProperty("null_value")
        val nullValue: String? = IntegerDataTypeDefaults.NULL_VALUE,
        val store: Boolean = IntegerDataTypeDefaults.STORE
) : DataType {
    override val type: String
        get() = "integer"
}
