package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class IndexPrefixes(
        @JsonProperty("min_chars")
        val minChars: Int? = 2,
        @JsonProperty("max_chars")
        val maxChars: Int? = 5
)
