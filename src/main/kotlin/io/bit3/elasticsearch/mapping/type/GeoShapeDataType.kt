package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class GeoShapeDataType(
        val tree: String = GeoShapeDataTypeDefaults.TREE,
        val precision: String = GeoShapeDataTypeDefaults.PRECISION,
        @JsonProperty("tree_levels")
        val treeLevels: String? = GeoShapeDataTypeDefaults.TREE_LEVELS,
        val strategy: String = GeoShapeDataTypeDefaults.STRATEGY,
        @JsonProperty("distance_error_pct")
        val distanceErrorPct: Double = GeoShapeDataTypeDefaults.DISTANCE_ERROR_PCT,
        val orientation: String = GeoShapeDataTypeDefaults.ORIENTATION,
        @JsonProperty("points_only")
        val pointsOnly: Boolean = GeoShapeDataTypeDefaults.POINTS_ONLY,
        @JsonProperty("ignore_malformed")
        val ignoreMalformed: Boolean = GeoShapeDataTypeDefaults.IGNORE_MALFORMED,
        @JsonProperty("ignore_z_value")
        val ignoreZValue: Boolean = GeoShapeDataTypeDefaults.IGNORE_ZVALUE
) : DataType {
    override val type: String
        get() = "geo_shape"
}
