package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class DateNanosDataType(
        val boost: Double = 1.0,
        @JsonProperty("doc_values")
        val docValues: Boolean = true,
        val format: String? = null,
        val locale: String? = null,
        @JsonProperty("ignore_malformed")
        val ignoreMalformed: Boolean = false,
        val index: Boolean = true,
        @JsonProperty("null_value")
        val nullValue: String? = null,
        val store: Boolean = false
) : DataType {
    override val type: String
        get() = "date_nanos"
}
