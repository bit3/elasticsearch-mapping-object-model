package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

class SimilaritySerializer : StdSerializer<Similarity>(Similarity::class.java) {

    override fun serialize(similarity: Similarity?, generator: JsonGenerator, provider: SerializerProvider) {
        if (null == similarity) {
            generator.writeNull()
        } else {
            generator.writeString(similarity.value)
        }
    }

}
