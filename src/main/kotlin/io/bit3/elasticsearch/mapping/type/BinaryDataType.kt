package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class BinaryDataType(
        @JsonProperty("doc_values")
        val docValues: Boolean = BinaryDataTypeDefaults.DOC_VALUES,
        val store: Boolean = BinaryDataTypeDefaults.STORE
) : DataType {
    override val type: String
        get() = "binary"
}
