package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonProperty

class GeoPointDataTypeDefaults {
    companion object {
        const val IGNORE_MALFORMED: Boolean = false
        const val IGNORE_ZVALUE: Boolean = true
        @JvmStatic
        val NULL_VALUE: String? = null
    }
}