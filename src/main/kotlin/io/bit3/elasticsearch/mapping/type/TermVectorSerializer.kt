package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

class TermVectorSerializer : StdSerializer<TermVector>(TermVector::class.java) {

    override fun serialize(termVector: TermVector?, generator: JsonGenerator, provider: SerializerProvider) {
        if (null == termVector) {
            generator.writeNull()
        } else {
            generator.writeString(termVector.toString().toLowerCase())
        }
    }

}
