package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.deser.std.StdDeserializer

class IndexOptionsDeserializer : StdDeserializer<IndexOptions>(IndexOptions::class.java) {

    override fun deserialize(parser: JsonParser, context: DeserializationContext): IndexOptions? {
        if (JsonToken.VALUE_NULL == parser.currentToken) {
            return null
        }

        return IndexOptions.valueOf(parser.valueAsString.toUpperCase())
    }

}
