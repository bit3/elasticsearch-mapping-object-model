package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

class BinaryDataTypeDefaults {
    companion object {
        const val DOC_VALUES: Boolean = false
        const val STORE: Boolean = false
    }
}
