package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class KeywordDataType(
        val boost: Double = 1.0,
        @JsonProperty("doc_values")
        val docValues: Boolean = true,
        @JsonProperty("eager_global_ordinals")
        val eagerGlobalOrdinals: Boolean = true,
        val fields: Map<String, DataType> = emptyMap(),
        @JsonProperty("ignore_above")
        val ignoreAbove: Int = 2147483647,
        val index: Boolean = true,
        @JsonProperty("index_options")
        val indexOptions: String? = null,
        val norms: Boolean = false,
        @JsonProperty("null_value")
        val nullValue: String? = null,
        val store: Boolean = false,
        val similarity: String = "BM25",
        val normalizer: String? = null,
        @JsonProperty("split_queries_on_whitespace")
        val splitQueriesOnWhitespace: Boolean = false
) : DataType {
    override val type: String
        get() = "keyword"
}
