package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonProperty

class FielddataFrequencyFilterDefaults {
    companion object {
        @JvmStatic
        val MIN: Double? = null

        @JvmStatic
        val MAX: Double? = null

        @JvmStatic
        val MIN_SEGMENT_SIZE: Int? = null
    }
}