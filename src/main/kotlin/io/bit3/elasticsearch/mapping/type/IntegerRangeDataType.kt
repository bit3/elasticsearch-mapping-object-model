package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class IntegerRangeDataType(
        val coerce: Boolean = true,
        val boost: Double = 1.0,
        val index: Boolean = true,
        val store: Boolean = false
) : DataType {
    override val type: String
        get() = "integer_range"
}
