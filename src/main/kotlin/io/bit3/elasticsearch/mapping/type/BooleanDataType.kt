package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class BooleanDataType(
        /**
         * Mapping field-level query time boosting. Accepts a floating point number, defaults to 1.0.
         */
        val boost: Double = BooleanDataTypeDefaults.BOOST,

        /**
         * Should the field be stored on disk in a column-stride fashion, so that it can later be used for sorting, aggregations, or scripting?
         * Accepts true (default) or false.
         */
        @JsonProperty("doc_values")
        val docValues: Boolean = BooleanDataTypeDefaults.DOC_VALUES,

        /**
         * Should the field be searchable? Accepts true (default) and false.
         */
        val index: Boolean = BooleanDataTypeDefaults.INDEX,

        /**
         * Accepts any of the true or false values listed above. The value is substituted for any explicit null values.
         * Defaults to null, which means the field is treated as missing.
         */
        @JsonProperty("null_value")
        val nullValue: Boolean? = BooleanDataTypeDefaults.NULL_VALUE,

        /**
         * Whether the field value should be stored and retrievable separately from the _source field. Accepts true or false (default).
         */
        val store: Boolean = BooleanDataTypeDefaults.STORE
) : DataType {
    override val type: String
        get() = "boolean"
}
