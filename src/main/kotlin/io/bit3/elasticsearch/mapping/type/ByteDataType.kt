package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class ByteDataType(
        val coerce: Boolean = ByteDataTypeDefaults.COERCE,
        val boost: Double = ByteDataTypeDefaults.BOOST,
        @JsonProperty("doc_values")
        val docValues: Boolean = ByteDataTypeDefaults.DOC_VALUES,
        @JsonProperty("ignore_malformed")
        val ignoreMalformed: Boolean = ByteDataTypeDefaults.IGNORE_MALFORMED,
        val index: Boolean = ByteDataTypeDefaults.INDEX,
        @JsonProperty("null_value")
        val nullValue: String? = ByteDataTypeDefaults.NULL_VALUE,
        val store: Boolean = ByteDataTypeDefaults.STORE
) : DataType {
    override val type: String
        get() = "byte"
}
