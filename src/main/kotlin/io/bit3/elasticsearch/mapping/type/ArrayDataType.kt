package io.bit3.elasticsearch.mapping.type

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class ArrayDataType(
        val dynamic: Boolean = ArrayDataTypeDefaults.DYNAMIC,
        val properties: Map<String, DataType> = ArrayDataTypeDefaults.PROPERTIES
) : DataType {
    override val type: String
        get() = "array"
}
