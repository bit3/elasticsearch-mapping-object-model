package io.bit3.elasticsearch.mapping

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import io.bit3.elasticsearch.mapping.settings.Settings
import io.bit3.elasticsearch.mapping.type.DataType
import kotlin.reflect.KClass

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class Mapping(
        val type: KClass<*>,
        val settings: Settings = Settings(),
        val properties: Map<String, DataType> = emptyMap()
)
