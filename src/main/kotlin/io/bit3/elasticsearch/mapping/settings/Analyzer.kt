package io.bit3.elasticsearch.mapping.settings

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
data class Analyzer(
        val type: String,
        val tokenizer: String? = null,
        @JsonProperty("search_analyzer")
        val searchAnalyzer: String? = null,
        @JsonProperty("search_quote_analyzer")
        val searchQuoteAnalyzer: String? = null,
        val filter: List<String> = emptyList()
)
