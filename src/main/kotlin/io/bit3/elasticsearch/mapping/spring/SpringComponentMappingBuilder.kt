package io.bit3.elasticsearch.mapping.spring

import io.bit3.elasticsearch.mapping.Mapping
import io.bit3.elasticsearch.mapping.annotation.Document
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
import org.springframework.core.env.Environment
import org.springframework.core.type.filter.AnnotationTypeFilter

class SpringComponentMappingBuilder(private val basePackage: String) {

    fun build(environment: Environment): List<Mapping> {
        val provider = ClassPathScanningCandidateComponentProvider(true, environment)
        provider.addIncludeFilter(AnnotationTypeFilter(Document::class.java))
        val candidates = provider.findCandidateComponents(basePackage)
        return candidates.map { candidate ->
            val beanClassName = candidate.beanClassName
            val documentClazz = javaClass.classLoader.loadClass(beanClassName)
            Mapping(documentClazz.kotlin)
        }
    }

}
