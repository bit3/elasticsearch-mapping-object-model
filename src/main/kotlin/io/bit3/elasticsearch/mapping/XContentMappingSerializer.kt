package io.bit3.elasticsearch.mapping

import io.bit3.elasticsearch.mapping.settings.Analyzer
import io.bit3.elasticsearch.mapping.settings.Settings
import io.bit3.elasticsearch.mapping.type.*
import org.elasticsearch.common.xcontent.XContentBuilder

class XContentMappingSerializer {

    /**
     * Build the mapping with the given [XContentBuilder].
     */
    fun build(builder: XContentBuilder, mapping: Mapping) {
        builder.startObject()
        buildSettings(builder, mapping)
        buildMappings(builder, mapping)
        builder.endObject()
    }

    private fun buildSettings(builder: XContentBuilder, mapping: Mapping) {
        val settings = mapping.settings

        val hasAnalyzers = settings.analysis.analyzer.isNotEmpty()

        if (!hasAnalyzers) {
            // no settings
            return
        }

        // start settings
        builder.startObject("settings")

        if (hasAnalyzers) {
            buildAnalysis(builder, settings)
        }

        // end settings
        builder.endObject()
    }

    private fun buildAnalysis(builder: XContentBuilder, settings: Settings) {
        // start settings -> analysis
        builder.startObject("analysis")

        buildAnalyzers(builder, settings)

        // end settings -> analysis
        builder.endObject()
    }

    private fun buildAnalyzers(builder: XContentBuilder, settings: Settings) {
        // start settings -> analysis -> analyzer
        builder.startObject("analyzer")

        settings.analysis.analyzer.forEach { name, analyzer ->
            buildAnalyzer(builder, name, analyzer)
        }

        // end settings -> analysis -> analyzer
        builder.endObject()
    }

    private fun buildAnalyzer(builder: XContentBuilder, name: String, analyzer: Analyzer) {
        builder.startObject(name)

        builder.field("type", analyzer.type)
        analyzer.tokenizer?.let {
            builder.field("tokenizer", it)
        }
        analyzer.searchAnalyzer?.let {
            builder.field("search_analyzer", it)
        }
        analyzer.searchQuoteAnalyzer?.let {
            builder.field("search_quote_analyzer", it)
        }
        if (analyzer.filter.isNotEmpty()) {
            builder.array("filter", analyzer.filter)
        }

        builder.endObject()
    }

    private fun buildMappings(builder: XContentBuilder, mapping: Mapping) {
        // start mappings
        builder.startObject("mappings")

        // properties
        builder.startObject("properties")
        buildProperties(builder, mapping.properties)
        builder.endObject()

        // end mappings
        builder.endObject()
    }

    private fun buildProperties(builder: XContentBuilder, properties: Map<String, DataType>) {
        properties.forEach { name, dataType ->
            buildProperty(builder, name, dataType)
        }
    }

    private fun buildProperty(builder: XContentBuilder, name: String, dataType: DataType) {
        builder.startObject(name)
        builder.field("type", dataType.type)

        if (dataType is AliasDataType) {
            buildAliasProperty(builder, dataType)
        } else if (dataType is ArrayDataType) {
            buildArrayProperty(builder, dataType)
        } else if (dataType is BinaryDataType) {
            buildBinaryProperty(builder, dataType)
        } else if (dataType is BooleanDataType) {
            buildBooleanProperty(builder, dataType)
        } else if (dataType is ByteDataType) {
            buildByteProperty(builder, dataType)
        } else if (dataType is DateDataType) {
            buildDateProperty(builder, dataType)
        } else if (dataType is DateNanosDataType) {
            buildDateNanosProperty(builder, dataType)
        } else if (dataType is DateRangeDataType) {
            buildDateRangeProperty(builder, dataType)
        } else if (dataType is DoubleDataType) {
            buildDoubleProperty(builder, dataType)
        } else if (dataType is DoubleRangeDataType) {
            buildDoubleRangeProperty(builder, dataType)
        } else if (dataType is FloatDataType) {
            buildFloatProperty(builder, dataType)
        } else if (dataType is FloatRangeDataType) {
            buildFloatRangeProperty(builder, dataType)
        } else if (dataType is GeoPointDataType) {
            buildGeoPointProperty(builder, dataType)
        } else if (dataType is GeoShapeDataType) {
            buildGeoShapeProperty(builder, dataType)
        } else if (dataType is HalfFloatDataType) {
            buildHalfFloatProperty(builder, dataType)
        } else if (dataType is IntegerDataType) {
            buildIntegerProperty(builder, dataType)
        } else if (dataType is IntegerRangeDataType) {
            buildIntegerRangeProperty(builder, dataType)
        } else if (dataType is IpDataType) {
            buildIpProperty(builder, dataType)
        } else if (dataType is IpRangeDataType) {
            buildIpRangeProperty(builder, dataType)
        } else if (dataType is KeywordDataType) {
            buildKeywordProperty(builder, dataType)
        } else if (dataType is LongDataType) {
            buildLongProperty(builder, dataType)
        } else if (dataType is LongRangeDataType) {
            buildLongRangeProperty(builder, dataType)
        } else if (dataType is NestedDataType) {
            buildNestedProperty(builder, dataType)
        } else if (dataType is ObjectDataType) {
            buildObjectProperty(builder, dataType)
        } else if (dataType is ScaledFloatDataType) {
            buildScaledFloatProperty(builder, dataType)
        } else if (dataType is ShortDataType) {
            buildShortProperty(builder, dataType)
        } else if (dataType is TextDataType) {
            buildTextProperty(builder, dataType)
        } else if (dataType is TokenCountDataType) {
            buildTokenCountProperty(builder, dataType)
        } else {
            throw IllegalArgumentException("Unsupported property \"$name\" of type \"$dataType\"")
        }

        builder.endObject()
    }

    private fun buildAliasProperty(builder: XContentBuilder, dataType: AliasDataType) {
        builder.field("path", dataType.path)
    }

    private fun buildArrayProperty(builder: XContentBuilder, dataType: ArrayDataType) {
        writeFieldIfNotDefault(builder, "dynamic", dataType.dynamic, true)
        writeFieldIfNotDefault(builder, "properties", dataType.properties, emptyMap())
    }

    private fun buildBinaryProperty(builder: XContentBuilder, dataType: BinaryDataType) {
        writeFieldIfNotDefault(builder, "doc_values", dataType.docValues, false)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun buildBooleanProperty(builder: XContentBuilder, dataType: BooleanDataType) {
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "doc_values", dataType.docValues, true)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "null_value", dataType.nullValue, null)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun buildByteProperty(builder: XContentBuilder, dataType: ByteDataType) {
        writeFieldIfNotDefault(builder, "coerce", dataType.coerce, true)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "doc_values", dataType.docValues, true)
        writeFieldIfNotDefault(builder, "ignore_malformed", dataType.ignoreMalformed, false)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "null_value", dataType.nullValue, null)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun buildDateProperty(builder: XContentBuilder, dataType: DateDataType) {
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "doc_values", dataType.docValues, true)
        writeFieldIfNotDefault(builder, "format", dataType.format, null)
        writeFieldIfNotDefault(builder, "locale", dataType.locale, null)
        writeFieldIfNotDefault(builder, "ignore_malformed", dataType.ignoreMalformed, false)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "null_value", dataType.nullValue, null)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun buildDateNanosProperty(builder: XContentBuilder, dataType: DateNanosDataType) {
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "doc_values", dataType.docValues, true)
        writeFieldIfNotDefault(builder, "format", dataType.format, null)
        writeFieldIfNotDefault(builder, "locale", dataType.locale, null)
        writeFieldIfNotDefault(builder, "ignore_malformed", dataType.ignoreMalformed, false)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "null_value", dataType.nullValue, null)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun buildDateRangeProperty(builder: XContentBuilder, dataType: DateRangeDataType) {
        writeFieldIfNotDefault(builder, "coerce", dataType.coerce, true)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun buildDoubleProperty(builder: XContentBuilder, dataType: DoubleDataType) {
        writeFieldIfNotDefault(builder, "coerce", dataType.coerce, true)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "docValues", dataType.docValues, true)
        writeFieldIfNotDefault(builder, "ignoreMalformed", dataType.ignoreMalformed, false)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "nullValue", dataType.nullValue, null)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun buildDoubleRangeProperty(builder: XContentBuilder, dataType: DoubleRangeDataType) {
        writeFieldIfNotDefault(builder, "coerce", dataType.coerce, true)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun buildFloatProperty(builder: XContentBuilder, dataType: FloatDataType) {
        writeFieldIfNotDefault(builder, "coerce", dataType.coerce, FloatDataTypeDefaults.COERCE)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, FloatDataTypeDefaults.BOOST)
        writeFieldIfNotDefault(builder, "docValues", dataType.docValues, FloatDataTypeDefaults.DOC_VALUES)
        writeFieldIfNotDefault(builder, "ignoreMalformed", dataType.ignoreMalformed, FloatDataTypeDefaults.IGNORE_MALFORMED)
        writeFieldIfNotDefault(builder, "index", dataType.index, FloatDataTypeDefaults.INDEX)
        writeFieldIfNotDefault(builder, "nullValue", dataType.nullValue, FloatDataTypeDefaults.NULL_VALUE)
        writeFieldIfNotDefault(builder, "store", dataType.store, FloatDataTypeDefaults.STORE)
    }

    private fun buildFloatRangeProperty(builder: XContentBuilder, dataType: FloatRangeDataType) {
        writeFieldIfNotDefault(builder, "coerce", dataType.coerce, true)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun buildGeoPointProperty(builder: XContentBuilder, dataType: GeoPointDataType) {
        writeFieldIfNotDefault(builder, "ignore_malformed", dataType.ignoreMalformed, GeoPointDataTypeDefaults.IGNORE_MALFORMED)
        writeFieldIfNotDefault(builder, "ignore_z_value", dataType.ignoreZValue, GeoPointDataTypeDefaults.IGNORE_ZVALUE)
        writeFieldIfNotDefault(builder, "null_value", dataType.nullValue, GeoPointDataTypeDefaults.NULL_VALUE)
    }

    private fun buildGeoShapeProperty(builder: XContentBuilder, dataType: GeoShapeDataType) {
        writeFieldIfNotDefault(builder, "tree", dataType.tree, GeoShapeDataTypeDefaults.TREE)
        writeFieldIfNotDefault(builder, "precision", dataType.precision, GeoShapeDataTypeDefaults.PRECISION)
        writeFieldIfNotDefault(builder, "tree_levels", dataType.treeLevels, GeoShapeDataTypeDefaults.TREE_LEVELS)
        writeFieldIfNotDefault(builder, "strategy", dataType.strategy, GeoShapeDataTypeDefaults.STRATEGY)
        writeFieldIfNotDefault(builder, "distance_error_pct", dataType.distanceErrorPct, GeoShapeDataTypeDefaults.DISTANCE_ERROR_PCT)
        writeFieldIfNotDefault(builder, "orientation", dataType.orientation, GeoShapeDataTypeDefaults.ORIENTATION)
        writeFieldIfNotDefault(builder, "points_only", dataType.pointsOnly, GeoShapeDataTypeDefaults.POINTS_ONLY)
        writeFieldIfNotDefault(builder, "ignore_malformed", dataType.ignoreMalformed, GeoShapeDataTypeDefaults.IGNORE_MALFORMED)
        writeFieldIfNotDefault(builder, "ignore_z_value", dataType.ignoreZValue, GeoShapeDataTypeDefaults.IGNORE_ZVALUE)
    }

    private fun buildHalfFloatProperty(builder: XContentBuilder, dataType: HalfFloatDataType) {
        writeFieldIfNotDefault(builder, "coerce", dataType.coerce, HalfFloatDataTypeDefaults.COERCE)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, HalfFloatDataTypeDefaults.BOOST)
        writeFieldIfNotDefault(builder, "doc_values", dataType.docValues, HalfFloatDataTypeDefaults.DOC_VALUES)
        writeFieldIfNotDefault(builder, "ignore_malformed", dataType.ignoreMalformed, HalfFloatDataTypeDefaults.IGNORE_MALFORMED)
        writeFieldIfNotDefault(builder, "index", dataType.index, HalfFloatDataTypeDefaults.INDEX)
        writeFieldIfNotDefault(builder, "null_value", dataType.nullValue, HalfFloatDataTypeDefaults.NULL_VALUE)
        writeFieldIfNotDefault(builder, "store", dataType.store, HalfFloatDataTypeDefaults.STORE)
    }

    private fun buildIntegerProperty(builder: XContentBuilder, dataType: IntegerDataType) {
        writeFieldIfNotDefault(builder, "coerce", dataType.coerce, IntegerDataTypeDefaults.COERCE)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, IntegerDataTypeDefaults.BOOST)
        writeFieldIfNotDefault(builder, "doc_values", dataType.docValues, IntegerDataTypeDefaults.DOC_VALUES)
        writeFieldIfNotDefault(builder, "ignore_malformed", dataType.ignoreMalformed, IntegerDataTypeDefaults.IGNORE_MALFORMED)
        writeFieldIfNotDefault(builder, "index", dataType.index, IntegerDataTypeDefaults.INDEX)
        writeFieldIfNotDefault(builder, "null_value", dataType.nullValue, IntegerDataTypeDefaults.NULL_VALUE)
        writeFieldIfNotDefault(builder, "store", dataType.store, IntegerDataTypeDefaults.STORE)
    }

    private fun buildIntegerRangeProperty(builder: XContentBuilder, dataType: IntegerRangeDataType) {
        writeFieldIfNotDefault(builder, "coerce", dataType.coerce, true)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun buildIpProperty(builder: XContentBuilder, dataType: IpDataType) {
        writeFieldIfNotDefault(builder, "boost", dataType.boost, IpDataTypeDefaults.BOOST)
        writeFieldIfNotDefault(builder, "doc_values", dataType.docValues, IpDataTypeDefaults.DOC_VALUES)
        writeFieldIfNotDefault(builder, "index", dataType.index, IpDataTypeDefaults.INDEX)
        writeFieldIfNotDefault(builder, "null_value", dataType.nullValue, IpDataTypeDefaults.NULL_VALUE)
        writeFieldIfNotDefault(builder, "store", dataType.store, IpDataTypeDefaults.STORE)
    }

    private fun buildIpRangeProperty(builder: XContentBuilder, dataType: IpRangeDataType) {
        writeFieldIfNotDefault(builder, "coerce", dataType.coerce, true)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun buildKeywordProperty(builder: XContentBuilder, dataType: KeywordDataType) {
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "doc_values", dataType.docValues, true)
        writeFieldIfNotDefault(builder, "eager_global_ordinals", dataType.eagerGlobalOrdinals, true)
        writeFieldIfNotDefault(builder, "fields", dataType.fields, emptyMap())
        writeFieldIfNotDefault(builder, "ignore_above", dataType.ignoreAbove, 2147483647)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "index_options", dataType.indexOptions, null)
        writeFieldIfNotDefault(builder, "norms", dataType.norms, false)
        writeFieldIfNotDefault(builder, "null_value", dataType.nullValue, null)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
        writeFieldIfNotDefault(builder, "similarity", dataType.similarity, "BM25")
        writeFieldIfNotDefault(builder, "normalizer", dataType.normalizer, null)
        writeFieldIfNotDefault(builder, "split_queries_on_whitespace", dataType.splitQueriesOnWhitespace, false)
    }

    private fun buildLongProperty(builder: XContentBuilder, dataType: LongDataType) {
        writeFieldIfNotDefault(builder, "coerce", dataType.coerce, true)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "doc_values", dataType.docValues, true)
        writeFieldIfNotDefault(builder, "ignore_malformed", dataType.ignoreMalformed, false)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "null_value", dataType.nullValue, null)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun buildLongRangeProperty(builder: XContentBuilder, dataType: LongRangeDataType) {
        writeFieldIfNotDefault(builder, "coerce", dataType.coerce, true)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun buildNestedProperty(builder: XContentBuilder, dataType: NestedDataType) {
        writeFieldIfNotDefault(builder, "dynamic", dataType.dynamic, true)
        writeFieldIfNotDefault(builder, "properties", dataType.properties, emptyMap())
    }

    private fun buildObjectProperty(builder: XContentBuilder, dataType: ObjectDataType) {
        writeFieldIfNotDefault(builder, "dynamic", dataType.dynamic, true)
        writeFieldIfNotDefault(builder, "enabled", dataType.enabled, true)
        writeFieldIfNotDefault(builder, "properties", dataType.properties, emptyMap())
    }

    private fun buildScaledFloatProperty(builder: XContentBuilder, dataType: ScaledFloatDataType) {
        builder.field("scaling_factor", dataType.scalingFactor)
        writeFieldIfNotDefault(builder, "coerce", dataType.coerce, true)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "doc_values", dataType.docValues, true)
        writeFieldIfNotDefault(builder, "ignore_malformed", dataType.ignoreMalformed, false)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "null_value", dataType.nullValue, null)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun buildShortProperty(builder: XContentBuilder, dataType: ShortDataType) {
        writeFieldIfNotDefault(builder, "coerce", dataType.coerce, true)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "doc_values", dataType.docValues, true)
        writeFieldIfNotDefault(builder, "ignore_malformed", dataType.ignoreMalformed, false)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "null_value", dataType.nullValue, null)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun buildTextProperty(builder: XContentBuilder, dataType: TextDataType) {
        val fielddataFrequencyFilter = dataType.fielddataFrequencyFilter
        val indexPrefixes = dataType.indexPrefixes

        writeFieldIfNotDefault(builder, "analyzer", dataType.analyzer, TextDataTypeDefaults.ANALYZER)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, TextDataTypeDefaults.BOOST)
        writeFieldIfNotDefault(builder, "eager_global_ordinals", dataType.eagerGlobalOrdinals, TextDataTypeDefaults.EAGER_GLOBAL_ORDINALS)
        writeFieldIfNotDefault(builder, "fielddata", dataType.fielddata, TextDataTypeDefaults.FIELDDATA)
        if (null != fielddataFrequencyFilter && fielddataFrequencyFilter != TextDataTypeDefaults.FIELDDATA_FREQUENCY_FILTER) {
            builder.startObject("fielddata_frequency_filter")
            writeFieldIfNotDefault(builder, "min", fielddataFrequencyFilter.min, FielddataFrequencyFilterDefaults.MIN)
            writeFieldIfNotDefault(builder, "max", fielddataFrequencyFilter.max, FielddataFrequencyFilterDefaults.MAX)
            writeFieldIfNotDefault(builder, "min_segment_size", fielddataFrequencyFilter.minSegmentSize, FielddataFrequencyFilterDefaults.MIN_SEGMENT_SIZE)
            builder.endObject()
        }
        writeFieldIfNotDefault(builder, "fields", dataType.fields, TextDataTypeDefaults.FIELDS)
        writeFieldIfNotDefault(builder, "index", dataType.index, TextDataTypeDefaults.INDEX)
        writeFieldIfNotDefault(builder, "index_options", dataType.indexOptions, TextDataTypeDefaults.INDEX_OPTIONS) { it.toString().toLowerCase() }
        if (null != indexPrefixes && indexPrefixes != TextDataTypeDefaults.INDEX_PREFIXES) {
            builder.startObject("index_prefixes")
            writeFieldIfNotDefault(builder, "min_chars", indexPrefixes.minChars, 2)
            writeFieldIfNotDefault(builder, "max_chars", indexPrefixes.maxChars, 5)
            builder.endObject()
        }

        writeFieldIfNotDefault(builder, "index_phrases", dataType.indexPhrases, TextDataTypeDefaults.INDEX_PHRASES)
        writeFieldIfNotDefault(builder, "norms", dataType.norms, TextDataTypeDefaults.NORMS)
        writeFieldIfNotDefault(builder, "position_increment_gap", dataType.positionIncrementGap, TextDataTypeDefaults.POSITION_INCREMENT_GAP)
        writeFieldIfNotDefault(builder, "store", dataType.store, TextDataTypeDefaults.STORE)
        writeFieldIfNotDefault(builder, "search_analyzer", dataType.searchAnalyzer, TextDataTypeDefaults.SEARCH_ANALYZER)
        writeFieldIfNotDefault(builder, "search_quote_analyzer", dataType.searchQuoteAnalyzer, TextDataTypeDefaults.SEARCH_QUOTE_ANALYZER)
        writeFieldIfNotDefault(builder, "similarity", dataType.similarity, TextDataTypeDefaults.SIMILARITY) { it.value }
        writeFieldIfNotDefault(builder, "term_vector", dataType.termVector, TextDataTypeDefaults.TERM_VECTOR) { it.toString().toLowerCase() }
    }

    private fun buildTokenCountProperty(builder: XContentBuilder, dataType: TokenCountDataType) {
        writeFieldIfNotDefault(builder, "analyzer", dataType.analyzer, null)
        writeFieldIfNotDefault(builder, "enable_position_increments", dataType.enablePositionIncrements, true)
        writeFieldIfNotDefault(builder, "boost", dataType.boost, 1.0)
        writeFieldIfNotDefault(builder, "doc_values", dataType.docValues, false)
        writeFieldIfNotDefault(builder, "index", dataType.index, true)
        writeFieldIfNotDefault(builder, "null_value", dataType.nullValue, null)
        writeFieldIfNotDefault(builder, "store", dataType.store, false)
    }

    private fun writeFieldIfNotDefault(builder: XContentBuilder, name: String, value: Map<String, DataType>?, default: Map<String, DataType>?) {
        if (null != value && value != default) {
            builder.startObject(name)
            buildProperties(builder, value)
            builder.endObject()
        }
    }

    private fun <T> writeFieldIfNotDefault(builder: XContentBuilder, name: String, value: T?, default: T?) {
        if (null != value && value != default) {
            builder.field(name, value)
        }
    }

    private fun <T, V> writeFieldIfNotDefault(builder: XContentBuilder, name: String, value: T?, default: T?, transformer: (T) -> V) {
        if (null != value && value != default) {
            builder.field(name, transformer(value))
        }
    }

}
