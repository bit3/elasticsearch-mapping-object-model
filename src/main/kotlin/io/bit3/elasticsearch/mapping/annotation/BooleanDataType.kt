package io.bit3.elasticsearch.mapping.annotation

@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.FUNCTION)
annotation class BooleanDataType(
        /**
         * The property name, if not defined use the field/method name.
         */
        val name: String = NULL,

        /**
         * Mapping field-level query time boosting. Accepts a floating point number, defaults to 1.0.
         */
        val boost: Double = 1.0,

        /**
         * Should the field be stored on disk in a column-stride fashion, so that it can later be used for sorting, aggregations, or scripting?
         * Accepts true (default) or false.
         */
        val docValues: Boolean = true,

        /**
         * Should the field be searchable? Accepts true (default) and false.
         */
        val index: Boolean = true,

        /**
         * Accepts any of the true or false values listed above. The value is substituted for any explicit null values.
         * Defaults to null, which means the field is treated as missing.
         */
        val nullValue: BooleanNullValue = BooleanNullValue.NULL,

        /**
         * Whether the field value should be stored and retrievable separately from the _source field. Accepts true or false (default).
         */
        val store: Boolean = false
)
