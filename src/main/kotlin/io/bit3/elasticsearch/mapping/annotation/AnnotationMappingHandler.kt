package io.bit3.elasticsearch.mapping.annotation

import io.bit3.elasticsearch.mapping.Mapping
import io.bit3.elasticsearch.mapping.MappingBuilder
import io.bit3.elasticsearch.mapping.MappingHandler
import io.bit3.elasticsearch.mapping.settings.Settings
import io.bit3.elasticsearch.mapping.type.*
import io.bit3.elasticsearch.mapping.type.AliasDataType
import io.bit3.elasticsearch.mapping.type.ArrayDataType
import io.bit3.elasticsearch.mapping.type.BinaryDataType
import io.bit3.elasticsearch.mapping.type.BooleanDataType
import io.bit3.elasticsearch.mapping.type.ByteDataType
import io.bit3.elasticsearch.mapping.type.DateDataType
import io.bit3.elasticsearch.mapping.type.DateNanosDataType
import io.bit3.elasticsearch.mapping.type.DateRangeDataType
import io.bit3.elasticsearch.mapping.type.DoubleDataType
import io.bit3.elasticsearch.mapping.type.DoubleRangeDataType
import io.bit3.elasticsearch.mapping.type.FielddataFrequencyFilter
import io.bit3.elasticsearch.mapping.type.FloatDataType
import io.bit3.elasticsearch.mapping.type.FloatRangeDataType
import io.bit3.elasticsearch.mapping.type.GeoPointDataType
import io.bit3.elasticsearch.mapping.type.GeoShapeDataType
import io.bit3.elasticsearch.mapping.type.HalfFloatDataType
import io.bit3.elasticsearch.mapping.type.IndexPrefixes
import io.bit3.elasticsearch.mapping.type.IntegerDataType
import io.bit3.elasticsearch.mapping.type.IntegerRangeDataType
import io.bit3.elasticsearch.mapping.type.IpDataType
import io.bit3.elasticsearch.mapping.type.IpRangeDataType
import io.bit3.elasticsearch.mapping.type.KeywordDataType
import io.bit3.elasticsearch.mapping.type.LongDataType
import io.bit3.elasticsearch.mapping.type.LongRangeDataType
import io.bit3.elasticsearch.mapping.type.NestedDataType
import io.bit3.elasticsearch.mapping.type.ObjectDataType
import io.bit3.elasticsearch.mapping.type.ScaledFloatDataType
import io.bit3.elasticsearch.mapping.type.ShortDataType
import io.bit3.elasticsearch.mapping.type.TextDataType
import io.bit3.elasticsearch.mapping.type.TokenCountDataType
import kotlin.reflect.KAnnotatedElement
import kotlin.reflect.KCallable
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.jvm.jvmErasure
import io.bit3.elasticsearch.mapping.annotation.AliasDataType as AliasDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.ArrayDataType as ArrayDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.BinaryDataType as BinaryDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.BooleanDataType as BooleanDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.ByteDataType as ByteDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.DateDataType as DateDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.DateNanosDataType as DateNanosDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.DateRangeDataType as DateRangeDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.DoubleDataType as DoubleDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.DoubleRangeDataType as DoubleRangeDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.FielddataFrequencyFilter as FielddataFrequencyFilterAnnotation
import io.bit3.elasticsearch.mapping.annotation.FloatDataType as FloatDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.FloatRangeDataType as FloatRangeDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.GeoPointDataType as GeoPointDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.GeoShapeDataType as GeoShapeDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.HalfFloatDataType as HalfFloatDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.IndexPrefixes as IndexPrefixesAnnotation
import io.bit3.elasticsearch.mapping.annotation.IntegerDataType as IntegerDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.IntegerRangeDataType as IntegerRangeDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.IpDataType as IpDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.IpRangeDataType as IpRangeDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.KeywordDataType as KeywordDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.LongDataType as LongDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.LongRangeDataType as LongRangeDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.NestedDataType as NestedDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.ObjectDataType as ObjectDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.ScaledFloatDataType as ScaledFloatDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.ShortDataType as ShortDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.TextDataType as TextDataTypeAnnotation
import io.bit3.elasticsearch.mapping.annotation.TokenCountDataType as TokenCountDataTypeAnnotation

class AnnotationMappingHandler : MappingHandler {

    override fun handleClass(clazz: KClass<*>, mapping: Mapping, builder: MappingBuilder): Mapping {
        val settings = buildSettings(clazz)
        return mapping.copy(settings = settings)
    }

    override fun handleProperty(property: KAnnotatedElement, name: String?, dataType: DataType?, mapping: Mapping, builder: MappingBuilder): Pair<String?, DataType>? {
        return buildPropertyFromAliasDataTypeAnnotation(property, name)
                ?: buildPropertyFromByteDataTypeAnnotation(property, name)
                ?: buildPropertyFromBooleanDataTypeAnnotation(property, name)
                ?: buildPropertyFromBinaryDataTypeAnnotation(property, name)
                ?: buildPropertyFromDateNanosDataTypeAnnotation(property, name)
                ?: buildPropertyFromDateRangeDataTypeAnnotation(property, name)
                ?: buildPropertyFromDateDataTypeAnnotation(property, name)
                ?: buildPropertyFromArrayDataTypeAnnotation(property, name, builder)
                ?: buildPropertyFromDoubleDataTypeAnnotation(property, name)
                ?: buildPropertyFromDoubleRangeDataTypeAnnotation(property, name)
                ?: buildPropertyFromFloatDataTypeAnnotation(property, name)
                ?: buildPropertyFromFloatRangeDataTypeAnnotation(property, name)
                ?: buildPropertyFromGeoPointDataTypeAnnotation(property, name)
                ?: buildPropertyFromGeoShapeDataTypeAnnotation(property, name)
                ?: buildPropertyFromHalfFloatDataTypeAnnotation(property, name)
                ?: buildPropertyFromIntegerDataTypeAnnotation(property, name)
                ?: buildPropertyFromIntegerRangeDataTypeAnnotation(property, name)
                ?: buildPropertyFromIpDataTypeAnnotation(property, name)
                ?: buildPropertyFromIpRangeDataTypeAnnotation(property, name)
                ?: buildPropertyFromKeywordDataTypeAnnotation(property, name, builder)
                ?: buildPropertyFromLongDataTypeAnnotation(property, name)
                ?: buildPropertyFromLongRangeDataTypeAnnotation(property, name)
                ?: buildPropertyFromNestedDataTypeAnnotation(property, name, builder)
                ?: buildPropertyFromObjectDataTypeAnnotation(property, name, builder)
                ?: buildPropertyFromScaledFloatDataTypeAnnotation(property, name)
                ?: buildPropertyFromShortDataTypeAnnotation(property, name)
                ?: buildPropertyFromTextDataTypeAnnotation(property, name, builder)
                ?: buildPropertyFromTokenCountDataTypeAnnotation(property, name)
    }

    private fun buildSettings(type: KClass<*>): Settings {
        val settings = Settings()

        val document = type.findAnnotation<Document>()
        if (null != document) {
            // TODO document
        }

        return settings
    }

    private fun buildPropertyFromAliasDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<AliasDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    AliasDataType(
                            it.path
                    )
            )
        }
    }

    private fun buildPropertyFromByteDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<ByteDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    ByteDataType(
                            coerce = it.coerce,
                            boost = it.boost,
                            docValues = it.docValues,
                            ignoreMalformed = it.ignoreMalformed,
                            index = it.index,
                            nullValue = it.nullValue,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromBooleanDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<BooleanDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    BooleanDataType(
                            boost = it.boost,
                            docValues = it.docValues,
                            index = it.index,
                            nullValue = it.nullValue.value,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromBinaryDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<BinaryDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    BinaryDataType(
                            docValues = it.docValues,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromDateNanosDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<DateNanosDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    DateNanosDataType(
                            boost = it.boost,
                            docValues = it.docValues,
                            format = it.format,
                            locale = it.locale,
                            ignoreMalformed = it.ignoreMalformed,
                            index = it.index,
                            nullValue = it.nullValue,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromDateRangeDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<DateRangeDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    DateRangeDataType(
                            coerce = it.coerce,
                            boost = it.boost,
                            index = it.index,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromDateDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<DateDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    DateDataType(
                            boost = it.boost,
                            docValues = it.docValues,
                            format = it.format.ifBlank { null },
                            locale = it.locale.ifBlank { null },
                            ignoreMalformed = it.ignoreMalformed,
                            index = it.index,
                            nullValue = it.nullValue.ifBlank { null },
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromArrayDataTypeAnnotation(property: KAnnotatedElement, name: String?, builder: MappingBuilder): Pair<String?, DataType>? {
        return property.findAnnotation<ArrayDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    ArrayDataType(
                            dynamic = it.dynamic,
                            properties = builder.buildProperties(it.properties)
                    )
            )
        }
    }

    private fun buildPropertyFromDoubleDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<DoubleDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    DoubleDataType(
                            coerce = it.coerce,
                            boost = it.boost,
                            docValues = it.docValues,
                            ignoreMalformed = it.ignoreMalformed,
                            index = it.index,
                            nullValue = it.nullValue,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromDoubleRangeDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<DoubleRangeDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    DoubleRangeDataType(
                            coerce = it.coerce,
                            boost = it.boost,
                            index = it.index,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromFloatDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<FloatDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    FloatDataType(
                            coerce = it.coerce,
                            boost = it.boost,
                            docValues = it.docValues,
                            ignoreMalformed = it.ignoreMalformed,
                            index = it.index,
                            nullValue = it.nullValue,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromFloatRangeDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<FloatRangeDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    FloatRangeDataType(
                            coerce = it.coerce,
                            boost = it.boost,
                            index = it.index,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromGeoPointDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<GeoPointDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    GeoPointDataType(
                            ignoreMalformed = it.ignoreMalformed,
                            ignoreZValue = it.ignoreZValue,
                            nullValue = it.nullValue
                    )
            )
        }
    }

    private fun buildPropertyFromGeoShapeDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<GeoShapeDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    GeoShapeDataType(
                            tree = it.tree,
                            precision = it.precision,
                            treeLevels = it.treeLevels,
                            strategy = it.strategy,
                            distanceErrorPct = it.distanceErrorPct,
                            orientation = it.orientation,
                            pointsOnly = it.pointsOnly,
                            ignoreMalformed = it.ignoreMalformed,
                            ignoreZValue = it.ignoreZValue
                    )
            )
        }
    }

    private fun buildPropertyFromHalfFloatDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<HalfFloatDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    HalfFloatDataType(
                            coerce = it.coerce,
                            boost = it.boost,
                            docValues = it.docValues,
                            ignoreMalformed = it.ignoreMalformed,
                            index = it.index,
                            nullValue = it.nullValue,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromIntegerDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<IntegerDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    IntegerDataType(
                            coerce = it.coerce,
                            boost = it.boost,
                            docValues = it.docValues,
                            ignoreMalformed = it.ignoreMalformed,
                            index = it.index,
                            nullValue = it.nullValue,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromIntegerRangeDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<IntegerRangeDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    IntegerRangeDataType(
                            coerce = it.coerce,
                            boost = it.boost,
                            index = it.index,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromIpDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<IpDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    IpDataType(
                            boost = it.boost,
                            docValues = it.docValues,
                            index = it.index,
                            nullValue = it.nullValue,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromIpRangeDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<IpRangeDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    IpRangeDataType(
                            coerce = it.coerce,
                            boost = it.boost,
                            index = it.index,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromKeywordDataTypeAnnotation(property: KAnnotatedElement, name: String?, builder: MappingBuilder): Pair<String?, DataType>? {
        return property.findAnnotation<KeywordDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    KeywordDataType(
                            boost = it.boost,
                            docValues = it.docValues,
                            eagerGlobalOrdinals = it.eagerGlobalOrdinals,
                            fields = builder.buildProperties(it.fields),
                            ignoreAbove = it.ignoreAbove,
                            index = it.index,
                            indexOptions = it.indexOptions,
                            norms = it.norms,
                            nullValue = it.nullValue,
                            store = it.store,
                            similarity = it.similarity,
                            normalizer = it.normalizer,
                            splitQueriesOnWhitespace = it.splitQueriesOnWhitespace
                    )
            )
        }
    }

    private fun buildPropertyFromLongDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<LongDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    LongDataType(
                            coerce = it.coerce,
                            boost = it.boost,
                            docValues = it.docValues,
                            ignoreMalformed = it.ignoreMalformed,
                            index = it.index,
                            nullValue = it.nullValue,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromLongRangeDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<LongRangeDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    LongRangeDataType(
                            coerce = it.coerce,
                            boost = it.boost,
                            index = it.index,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromNestedDataTypeAnnotation(property: KAnnotatedElement, name: String?, builder: MappingBuilder): Pair<String?, DataType>? {
        return property.findAnnotation<NestedDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    NestedDataType(
                            dynamic = it.dynamic,
                            properties = builder.buildProperties(inferSubType(property, it.properties))
                    )
            )
        }
    }

    private fun buildPropertyFromObjectDataTypeAnnotation(property: KAnnotatedElement, name: String?, builder: MappingBuilder): Pair<String?, DataType>? {
        return property.findAnnotation<ObjectDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    ObjectDataType(
                            dynamic = it.dynamic,
                            enabled = it.enabled,
                            properties = builder.buildProperties(inferSubType(property, it.properties))
                    )
            )
        }
    }

    private fun buildPropertyFromScaledFloatDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<ScaledFloatDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    ScaledFloatDataType(
                            scalingFactor = it.scalingFactor,
                            coerce = it.coerce,
                            boost = it.boost,
                            docValues = it.docValues,
                            ignoreMalformed = it.ignoreMalformed,
                            index = it.index,
                            nullValue = it.nullValue,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromShortDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<ShortDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    ShortDataType(
                            coerce = it.coerce,
                            boost = it.boost,
                            docValues = it.docValues,
                            ignoreMalformed = it.ignoreMalformed,
                            index = it.index,
                            nullValue = it.nullValue,
                            store = it.store
                    )
            )
        }
    }

    private fun buildPropertyFromTextDataTypeAnnotation(property: KAnnotatedElement, name: String?, builder: MappingBuilder): Pair<String?, DataType>? {
        return property.findAnnotation<TextDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    TextDataType(
                            analyzer = it.analyzer,
                            boost = it.boost,
                            eagerGlobalOrdinals = it.eagerGlobalOrdinals,
                            fielddata = it.fielddata,
                            fielddataFrequencyFilter = buildFielddataFrequencyFilter(it.fielddataFrequencyFilter),
                            fields = builder.buildProperties(inferSubType(property, it.fields)),
                            index = it.index,
                            indexOptions = it.indexOptions,
                            indexPrefixes = buildIndexPrefixes(it.indexPrefixes),
                            indexPhrases = it.indexPhrases,
                            norms = it.norms,
                            positionIncrementGap = it.positionIncrementGap.valueOrNull()?.toDouble(),
                            store = it.store,
                            searchAnalyzer = it.searchAnalyzer,
                            searchQuoteAnalyzer = it.searchQuoteAnalyzer,
                            similarity = it.similarity,
                            termVector = it.term_vector
                    )
            )
        }
    }

    private fun buildFielddataFrequencyFilter(fielddataFrequencyFilter: FielddataFrequencyFilterAnnotation): FielddataFrequencyFilter? {
        val min = fielddataFrequencyFilter.min.valueOrNull()?.toDouble()
        val max = fielddataFrequencyFilter.max.valueOrNull()?.toDouble()
        val minSegmentSize = fielddataFrequencyFilter.minSegmentSize.valueOrNull()?.toInt()

        return if (null == min && null == max && null == minSegmentSize) null
        else FielddataFrequencyFilter(min, max, minSegmentSize)
    }

    private fun buildIndexPrefixes(indexPrefixes: IndexPrefixesAnnotation): IndexPrefixes {
        val minChars = indexPrefixes.minChars.let {
            if (Int.MIN_VALUE == it) null
            else it
        }
        val maxChars = indexPrefixes.maxChars.let {
            if (Int.MIN_VALUE == it) null
            else it
        }

        return IndexPrefixes(minChars, maxChars)
    }

    private fun buildPropertyFromTokenCountDataTypeAnnotation(property: KAnnotatedElement, name: String?): Pair<String?, DataType>? {
        return property.findAnnotation<TokenCountDataTypeAnnotation>()?.let {
            return Pair(
                    it.name.valueOrNull() ?: name,
                    TokenCountDataType(
                            analyzer = it.analyzer,
                            enablePositionIncrements = it.enablePositionIncrements,
                            boost = it.boost,
                            docValues = it.docValues,
                            index = it.index,
                            nullValue = it.nullValue.valueOrNull()?.toBigDecimal(),
                            store = it.store
                    )
            )
        }
    }

    private fun inferSubType(property: KAnnotatedElement, declaredType: KClass<*> = Object::class): KClass<*> {
        return if (Object::class != declaredType) declaredType
        else if (property is KCallable<*>) property.returnType.jvmErasure
        else throw IllegalArgumentException("Cannot determine nested type of $property")
    }

}