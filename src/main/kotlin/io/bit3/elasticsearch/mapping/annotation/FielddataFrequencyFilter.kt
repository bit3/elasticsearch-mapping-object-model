package io.bit3.elasticsearch.mapping.annotation

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

annotation class FielddataFrequencyFilter(
        val min: String = NULL,
        val max: String = NULL,
        val minSegmentSize: String = NULL
)
