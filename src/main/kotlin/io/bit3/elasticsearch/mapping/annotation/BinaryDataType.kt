package io.bit3.elasticsearch.mapping.annotation

@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.FUNCTION)
annotation class BinaryDataType(
        /**
         * The property name, if not defined use the field/method name.
         */
        val name: String = NULL,
        val docValues: Boolean = false,
        val store: Boolean = false
)
