package io.bit3.elasticsearch.mapping.annotation

import kotlin.reflect.KClass

@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.FUNCTION)
annotation class KeywordDataType(
        /**
         * The property name, if not defined use the field/method name.
         */
        val name: String = NULL,
        val boost: Double = 1.0,
        val docValues: Boolean = true,
        val eagerGlobalOrdinals: Boolean = true,
        val fields: KClass<*> = Object::class,
        val ignoreAbove: Int = 2147483647,
        val index: Boolean = true,
        val indexOptions: String = NULL,
        val norms: Boolean = false,
        val nullValue: String = NULL,
        val store: Boolean = false,
        val similarity: String = "BM25",
        val normalizer: String = NULL,
        val splitQueriesOnWhitespace: Boolean = false
)
