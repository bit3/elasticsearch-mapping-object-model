package io.bit3.elasticsearch.mapping.annotation

@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.FUNCTION)
annotation class GeoShapeDataType(
        /**
         * The property name, if not defined use the field/method name.
         */
        val name: String = NULL,
        val tree: String = "quadtree",
        val precision: String = "50m",
        val treeLevels: String = NULL,
        val strategy: String = "recursive",
        val distanceErrorPct: Double = .025,
        val orientation: String = "ccw",
        val pointsOnly: Boolean = false,
        val ignoreMalformed: Boolean = false,
        val ignoreZValue: Boolean = true
)
