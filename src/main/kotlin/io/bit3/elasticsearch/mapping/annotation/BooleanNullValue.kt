package io.bit3.elasticsearch.mapping.annotation

enum class BooleanNullValue(val value: Boolean?) {
    NULL(null),
    TRUE(true),
    FALSE(false)
}