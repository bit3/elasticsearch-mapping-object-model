package io.bit3.elasticsearch.mapping.annotation

import io.bit3.elasticsearch.mapping.type.IndexOptions
import io.bit3.elasticsearch.mapping.type.Similarity
import io.bit3.elasticsearch.mapping.type.TermVector
import kotlin.reflect.KClass

@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.FUNCTION)
annotation class TextDataType(
        /**
         * The property name, if not defined use the field/method name.
         */
        val name: String = NULL,
        val analyzer: String = NULL,
        val boost: Double = 1.0,
        val eagerGlobalOrdinals: Boolean = false,
        val fielddata: Boolean = false,
        val fielddataFrequencyFilter: FielddataFrequencyFilter = FielddataFrequencyFilter(),
        val fields: KClass<*> = Object::class,
        val index: Boolean = true,
        val indexOptions: IndexOptions = IndexOptions.POSITIONS,
        val indexPrefixes: IndexPrefixes = IndexPrefixes(),
        val indexPhrases: Boolean = false,
        val norms: Boolean = true,

        /**
         * The number of fake term position which should be inserted between each element of an array of strings.
         * Defaults to the position_increment_gap configured on the analyzer which defaults to 100.
         * 100 was chosen because it prevents phrase queries with reasonably large slops (less than 100) from matching terms across field values.
         */
        val positionIncrementGap: String = NULL,

        /**
         * Whether the field value should be stored and retrievable separately from the _source field. Accepts true or false (default).
         */
        val store: Boolean = false,

        /**
         * The analyzer that should be used at search time on analyzed fields. Defaults to the `analyzer` setting.
         */
        val searchAnalyzer: String = NULL,

        /**
         * The analyzer that should be used at search time when a phrase is encountered. Defaults to the `search_analyzer` setting.
         */
        val searchQuoteAnalyzer: String = NULL,

        /**
         * Which scoring algorithm or similarity should be used. Defaults to [Similarity.BM25].
         */
        val similarity: Similarity = Similarity.BM25,

        /**
         * Whether term vectors should be stored for an analyzed field. Defaults to no.
         */
        val term_vector: TermVector = TermVector.NO
)
