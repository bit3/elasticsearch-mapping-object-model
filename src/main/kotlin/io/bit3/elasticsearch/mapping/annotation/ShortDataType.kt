package io.bit3.elasticsearch.mapping.annotation

@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.FUNCTION)
annotation class ShortDataType(
        /**
         * The property name, if not defined use the field/method name.
         */
        val name: String = NULL,
        val coerce: Boolean = true,
        val boost: Double = 1.0,
        val docValues: Boolean = true,
        val ignoreMalformed: Boolean = false,
        val index: Boolean = true,
        val nullValue: String = NULL,
        val store: Boolean = false
)
