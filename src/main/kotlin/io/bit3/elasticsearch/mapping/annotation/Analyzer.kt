package io.bit3.elasticsearch.mapping.annotation

annotation class Analyzer(
        val name: String,
        val type: String,
        val tokenizer: String = "",
        val searchAnalyzer: String = "",
        val searchQuoteAnalyzer: String = "",
        val filter: Array<String> = []
)
