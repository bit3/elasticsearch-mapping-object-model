package io.bit3.elasticsearch.mapping.annotation

@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.FUNCTION)
annotation class TokenCountDataType(
        /**
         * The property name, if not defined use the field/method name.
         */
        val name: String = NULL,

        /**
         * The analyzer which should be used to analyze the string value. Required. For best performance, use an analyzer without token filters.
         */
        val analyzer: String = NULL,

        /**
         * Indicates if position increments should be counted. Set to false if you don’t want to count tokens removed by analyzer filters (like stop). Defaults to true.
         */
        val enablePositionIncrements: Boolean = true,

        /**
         * Mapping field-level query time boosting. Accepts a floating point number, defaults to 1.0.
         */
        val boost: Double = 1.0,

        /**
         * Should the field be stored on disk in a column-stride fashion, so that it can later be used for sorting, aggregations, or scripting? Accepts true (default) or false.
         */
        val docValues: Boolean = false,

        /**
         * Should the field be searchable? Accepts true (default) and false.
         */
        val index: Boolean = true,

        /**
         * Accepts a numeric value of the same type as the field which is substituted for any explicit null values.
         * Defaults to null, which means the field is treated as missing.
         */
        val nullValue: String = NULL,

        /**
         * Whether the field value should be stored and retrievable separately from the _source field. Accepts true or false (default).
         */
        val store: Boolean = false
)
