package io.bit3.elasticsearch.mapping.annotation

@Target(AnnotationTarget.CLASS)
annotation class Document(
        val analyzer: Array<Analyzer> = []
)
