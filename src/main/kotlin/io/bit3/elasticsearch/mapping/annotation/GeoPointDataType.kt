package io.bit3.elasticsearch.mapping.annotation

@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.FUNCTION)
annotation class GeoPointDataType(
        /**
         * The property name, if not defined use the field/method name.
         */
        val name: String = NULL,
        val ignoreMalformed: Boolean = false,
        val ignoreZValue: Boolean = true,
        val nullValue: String = NULL
)
