package io.bit3.elasticsearch.mapping.annotation

import kotlin.reflect.KClass

@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY, AnnotationTarget.FUNCTION)
annotation class NestedDataType(
        /**
         * The property name, if not defined use the field/method name.
         */
        val name: String = NULL,
        val dynamic: Boolean = true,
        val properties: KClass<*> = Object::class
)
