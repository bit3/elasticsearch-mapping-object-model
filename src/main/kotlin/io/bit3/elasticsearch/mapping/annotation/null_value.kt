package io.bit3.elasticsearch.mapping.annotation

const val NULL = "io.bit3.elasticsearch.mapping.annotation.NULL_VALUE"

fun String?.valueOrNull(): String? {
    return if (NULL == this) null
    else this
}
