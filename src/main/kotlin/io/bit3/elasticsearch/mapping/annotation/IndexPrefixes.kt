package io.bit3.elasticsearch.mapping.annotation

annotation class IndexPrefixes(
        val minChars: Int = Int.MIN_VALUE,
        val maxChars: Int = Int.MIN_VALUE
)