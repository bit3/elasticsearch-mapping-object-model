package io.bit3.elasticsearch.mapping

import io.bit3.elasticsearch.mapping.type.DataType
import kotlin.reflect.KAnnotatedElement
import kotlin.reflect.KClass

interface MappingHandler {

    /**
     * Handle the class mapping.
     *
     * @param mapping The mapping object.
     * @param builder The mapping builder object.
     * @return Return a modified version of `mapping`.
     */
    fun handleClass(clazz: KClass<*>, mapping: Mapping, builder: MappingBuilder): Mapping = mapping

    /**
     * Handle the property of the annotated element.
     *
     * @param property The annotated element.
     * @param name The name that was determined by a previous handler.
     * @param dataType The [DataType] that was determined by a previous handler.
     * @param mapping The mapping object.
     * @param builder The mapping builder object.
     * @return Return `null` if this handler cannot handle this property.
     * Otherwise return a pair of name and [DataType].
     * It is save to return `null`, even when name and/or dataType is present!
     */
    fun handleProperty(property: KAnnotatedElement, name: String?, dataType: DataType?, mapping: Mapping, builder: MappingBuilder): Pair<String?, DataType>? = null

}
