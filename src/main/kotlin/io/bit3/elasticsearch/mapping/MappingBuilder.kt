package io.bit3.elasticsearch.mapping

import io.bit3.elasticsearch.mapping.type.DataType
import java.util.*
import javax.annotation.Priority
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.memberFunctions
import kotlin.reflect.full.memberProperties

class MappingBuilder(private val handlers: List<MappingHandler>) {

    companion object {
        const val DEFAULT_PRIORITY = Int.MAX_VALUE / 2

        fun loadMappingProcessorServices(): List<MappingHandler> {
            return ServiceLoader.load(MappingHandler::class.java).sortedBy { getPriority(it) }
        }

        private fun getPriority(`object`: Any): Int {
            return `object`::class.findAnnotation<Priority>()?.value ?: DEFAULT_PRIORITY
        }
    }

    constructor() : this(loadMappingProcessorServices())

    fun buildMapping(type: Class<*>): Mapping = buildMapping(type.kotlin)

    fun buildMapping(type: KClass<*>): Mapping {
        var mapping = Mapping(type)
        mapping = processClass(type, mapping)
        mapping = processProperties(type, mapping)
        return mapping
    }

    private fun processClass(type: KClass<*>, mapping: Mapping): Mapping {
        var m = mapping
        handlers.forEach { processor ->
            m = processor.handleClass(type, m, this)
        }
        return m
    }

    /**
     * Process only the properties of the given type.
     */
    fun buildProperties(type: Class<*>): Map<String, DataType> = buildProperties(type.kotlin)

    /**
     * Process only the properties of the given type.
     */
    fun buildProperties(type: KClass<*>): Map<String, DataType> {
        var mapping = Mapping(type)
        mapping = processProperties(type, mapping)
        return mapping.properties
    }

    private fun processProperties(type: KClass<*>, mapping: Mapping): Mapping {
        var m = mapping
        val properties = m.properties.toMutableMap()

        type.memberProperties.forEach { property ->
            var name: String? = null
            var dataType: DataType? = null

            handlers.forEach { processor ->
                val pair = processor.handleProperty(property, name, dataType, m, this)
                if (null != pair) {
                    name = pair.first
                    dataType = pair.second
                }
            }

            dataType?.let {
                properties.put(name ?: property.name, it)
                m = m.copy(properties = properties)
            }
        }

        type.memberFunctions.forEach { function ->
            var name: String? = null
            var dataType: DataType? = null

            handlers.forEach { processor ->
                val pair = processor.handleProperty(function, name, dataType, m, this)
                if (null != pair) {
                    name = pair.first
                    dataType = pair.second
                }
            }

            dataType?.let {
                properties.put(name ?: functionToPropertyName(function.name), it)
                m = m.copy(properties = properties)
            }
        }

        return m.copy(properties = properties.toMap())
    }

    private fun functionToPropertyName(name: String): String {
        return if (name.length >= 4 && name.startsWith("get")) name[3].toLowerCase() + name.substring(4)
        else if (name.length >= 3 && name.startsWith("is")) name[2].toLowerCase() + name.substring(3)
        else name
    }

}
