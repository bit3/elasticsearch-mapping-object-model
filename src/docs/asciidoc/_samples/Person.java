import io.bit3.elasticsearch.mapping.type.KeywordDataType
import io.bit3.elasticsearch.mapping.type.TextDataType

class Person {

    @TextDataType
    private String name;

    @KeywordDataType
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
