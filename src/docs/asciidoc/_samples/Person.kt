import io.bit3.elasticsearch.mapping.type.KeywordDataType
import io.bit3.elasticsearch.mapping.type.TextDataType

data class Person(
    @TextDataType
    val name: String,

    @KeywordDataType
    val email: String
)
